#!/bin/bash
#SCRIPT_DIR=`getScriptDir $0`
HE_DIR=$1
HRC_USER=$2
HRC_HOST=$3
lastrundate=$(date +"%Y-%m-%dT%T.%3NZEDT")
echo "BCBSNE SH : bcbs_corr_bam_updater.sh start"
mkdir -p $HE_DIR/bcbsne_corr_bam
cp -n bcbsagebump.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsmembertermination.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbssubscribertermination.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsclaimdelay15days.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsgroupbillduereminder.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsdentalwaitperiod.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsmissingssn12months.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsmissingssn6months.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsmissingssn7months.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbssubscriberauthrelease.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsmemberauthrelease.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsindividualreminderletter.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsnewbornletter.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsvoidinsufffundsaccount.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsvoidinsufficientfundsmember.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsgroupdeliquent.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsdisableddependentreverification.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbswaitingperiodletter.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbslateenrolleedentalwaits.json $HE_DIR/bcbsne_corr_bam/.
cp -n bcbsidcard.json $HE_DIR/bcbsne_corr_bam/.

cd $HE_DIR/bcbsne_corr_bam
jsons_to_set=($(grep -rl SSSS-MM-DDTHH:MM:SS.SSSZEDT))
#jsons_to_set=($(grep -rl esURL))
grep -rl SSSS-MM-DDTHH:MM:SS.SSSZEDT| xargs sed -i "s|SSSS-MM-DDTHH:MM:SS.SSSZEDT|${lastrundate}|g"
grep -rl EEEE-MM-DDTHH:MM:SS.SSSZEDT| xargs sed -i "s|EEEE-MM-DDTHH:MM:SS.SSSZEDT|${lastrundate}|g"

echo "BCBSNE SH : Setting last-run-entry $jsons_to_set"
for i in "${jsons_to_set[@]}"
do
	ssh -o BatchMode=yes -oPasswordAuthentication=no -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p 8101 $HRC_USER@$HRC_HOST hrc:last-run-entry file:bcbsne_corr_bam//$i
done
echo "BCBSNE SH : bcbs_corr_bam_updater.sh end"
