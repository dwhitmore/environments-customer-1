#!/bin/bash

PROPERTY_FILE="{{ pushbutton_properties_path }}/payor/bcbsaz_payor_env.properties"

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

payorUser=$(getProperty payorUser)
payorHost=$(getProperty payorHost)

echo "Copying application.properties to the Weblogic Server."
scp {{ pushbutton_properties_path }}/custom/application.properties ${payorUser}@${payorHost}:psTools/heAccelerator/providerContract/

echo "Starting Application."
ssh -q ${payorUser}@${payorHost} "nohup ~/psTools/heAccelerator/providerContract/start.sh > ~/psTools/heAccelerator/providerContract/logs/providerContractTool.log 2>&1 &"

exit 0
